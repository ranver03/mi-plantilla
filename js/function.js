$(document).ready(function(){
     var flag = true;
     var scroll;
     var ventana = $(window).height();

     $("#banner-parallax").css({"height": ventana + "px"});
     $("#slogan").css({"margin-top": ventana * 0.20 +"px"});
     $("#propuesta").css({"margin-top": ventana * 0.65 +"px"});
     $("#boton-started").css({"margin-top": ventana * 0.85 + "px"});

     $("#boton-started").on("click", function(){
     var posicion = $("#articulos-ini").offset().top;
     $("html, body").animate({
        scrollTop: posicion - 90 + "px"
     }, 2000);
     });


     $("#li-servicios").on("click", function(){
     var posicion = $("#menubtn").offset().top;
     $("html, body").animate({
        scrollTop: posicion - 90 + "px"
     }, 2000);
     });
     $(window).scroll(function(){
          scroll = $(window).scrollTop();
          var posicion = $("#articulos-ini").offset().top;
          if(scroll > posicion -120){
               if (flag){
                    $("#cabecera").removeClass("cabecera");
                    $("#cabecera").addClass("cabecera-scroll");
                    flag = false;
               }
          }else{
               if (!flag){
                    $("#cabecera").removeClass("cabecera-scroll");
                    $("#cabecera").addClass("cabecera");
                    flag = true;
               }
          }
     });

     $("#btn-empezemos").mouseover(function(event){
          $("#btn-empezemos").addClass("animated swing");
     });

     $("#btn-empezemos").mouseout(function(event){
          $("#btn-empezemos").removeClass("animated swing");
     });

     $("nav > ul > li").mouseover(function(event){
          $("#cabecera").addClass("cabecera-scroll");
          $("#cabecera").removeClass("cabecera");
     });

     $("nav > ul > li").mouseout(function(event){
          scroll = $(window).scrollTop();
          var posicion = $("#articulos-ini").offset().top;
          $("#cabecera").addClass("cabecera");

          if(scroll < posicion -100){
               $("#cabecera").removeClass("cabecera-scroll");
          }
     });


});
